package qatest.qatest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({EmailTest.class})
public class TesteRegressaoEmail
{
	/**
	 * Para executar os testes duas vezes ao dia. O ideal é fazer uso de 
	 * uma ferramenta de integração contínua (por exemplo o Jenkins). Com a ferramenta 
	 * é possível configurar o horário da execução dos testes de regressão.
	 * O ideal é sempre rodar os testes de regressão antes de um commit e configurar a 
	 * ferramenta de integração contínua para execução dos testes com o tempo desejado.
	 */

}
