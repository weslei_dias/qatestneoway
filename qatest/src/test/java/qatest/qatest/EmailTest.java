package qatest.qatest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import junit.framework.TestCase;

/**
 * Teste de Unidade da classe {@link Email}.
 * 
 * @author Wesley Dias
 *
 */
public class EmailTest extends TestCase {

	private Email email;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		email = new Email();
	}

	public void testarEmailValido() {

		// Testando com o metódo que usa expressão regular.
		String enderecoEmail = "abcd@teste.com";
		assertTrue("O email " + enderecoEmail + " deveria válido",
				email.isEmailValido(enderecoEmail));
		// Testando com a classe do commons validator.
		assertTrue("O email " + enderecoEmail + " deveria ser válido",
				email.isEmailValidoValidator(enderecoEmail));
	}

	public void testarDominioEmail() {

		String emailOrg = "weslei@weslei.org";
		assertTrue("O email " + emailOrg + " deveria ser válido",
				email.isEmailValido(emailOrg));

		String emailCom = "weslei@weslei.com";
		assertTrue("O email " + emailCom + " deveria ser válido",
				email.isEmailValidoValidator(emailCom));

		String emailNet = "weslei@weslei.net";
		assertTrue("O email " + emailNet + " deveria ser válido",
				email.isEmailValido(emailNet));

		String emailInfo = "weslei@weslei.info";
		assertTrue("O email " + emailOrg + " deveria ser válido",
				email.isEmailValidoValidator(emailInfo));

		String emailSemDominio = "weslei@weslei.";
		assertFalse("O email " + emailSemDominio + " deveria ser inválido",
				email.isEmailValido(emailSemDominio));

		String emailDominioIncompleto = "weslei@weslei.c";
		assertFalse("O email " + emailDominioIncompleto
				+ " deveria ser inválido",
				email.isEmailValidoValidator(emailDominioIncompleto));
	}

	public void testarEmailComTraco() {
		String emailComCaractereDominio = "weslei-dias@weslei-dias.-com";
		assertFalse("O email " + emailComCaractereDominio
				+ " deveria ser inválido",
				email.isEmailValido(emailComCaractereDominio));

		String emailComCaractereDominio2 = "weslei-dias@weslei-dias.c-om";
		assertFalse("O email " + emailComCaractereDominio2
				+ " deveria ser inválido",
				email.isEmailValidoValidator(emailComCaractereDominio2));
	}

	public void testarEmailComEspaco() {
		String emailComEspaco1 = "wesleidias @teste.org";
		assertFalse("O email " + emailComEspaco1 + " deveria ser inválido.",
				email.isEmailValido(emailComEspaco1));

		assertFalse(email.isEmailValidoValidator("weslei@ teste.org"));

		assertFalse(email.isEmailValido("wes lei@teste.org "));

		assertFalse(email.isEmailValidoValidator("weslei@tes te.org "));
	}

	public void testarEmailArquivo() {
		try {
			File file = new File("resources/arquivo.txt");
			BufferedReader br = new BufferedReader(new FileReader(
					file.getAbsolutePath()));
			while (br.ready()) {
				String emailRetornado = br.readLine();

				validacaoExpressaoRegular(emailRetornado);
				validacaoComvalidator(emailRetornado);
			}
			br.close();
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Arquivo não encontrado", e);
		} catch (IOException e) {
			throw new RuntimeException("Erro ao abrir o arquivo", e);
		}
	}

	private void validacaoComvalidator(String emailRetornado) {

		boolean isEmailvalido = email.isEmailValido(emailRetornado);

		System.out.println("###########ÍNICIO VALIDAÇÃO VALIDATOR###########");

		if (isEmailvalido) {
			System.out.println("O email " + emailRetornado + " é válido!");
		} else {
			System.out.println("O email " + emailRetornado + " é inválido!");

		}

		System.out.println("###########FIM VALIDAÇÃO VALIDATOR###########");

	}

	private void validacaoExpressaoRegular(String emailRetornado) {
		boolean isEmailvalido = email.isEmailValido(emailRetornado);

		System.out
				.println("###########ÍNICIO VALIDAÇÃO EXPRESSÃO REGULAR###########");

		if (isEmailvalido) {
			System.out.println("O email " + emailRetornado + " é válido!");
		} else {
			System.out.println("O email " + emailRetornado + " é inválido!");

		}

		System.out
				.println("###########FIM VALIDAÇÃO EXPRESSÃO REGULAR###########");
	}
}
