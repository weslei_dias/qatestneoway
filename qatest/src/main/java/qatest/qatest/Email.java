package qatest.qatest;

import java.util.regex.Pattern;

import org.apache.commons.validator.routines.EmailValidator;

/**
 * Classe de validação de emails.
 * @author Wesley Dias
 *
 */
public class Email {
	
	/**
	 * Usando a classe EmailValidator do commons validator.
	 * @param email utilizado
	 * @return se um email é válido ou inválido.
	 */
	public boolean isEmailValidoValidator(String email) {
		EmailValidator emailValidator = EmailValidator.getInstance();
		return emailValidator.isValid(email);
	}
	
	/**
	 * Usando expressão regular.
	 * @param email utilizado.
	 * @return se um email é valido ou falso.
	 */
	public boolean isEmailValido(String email) {
		String expressaoRegular = "[a-zA-Z0-9]{1,}[@]{1}[a-z]{5,}[.]{1}+[a-z]{3}";
		boolean isEmailValido = Pattern.matches(expressaoRegular, email);
		return isEmailValido;
	}

}
